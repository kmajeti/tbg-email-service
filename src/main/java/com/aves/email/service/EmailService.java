
package com.aves.email.service;


import com.aves.email.vo.ResetPasswordEmailRequest;
import com.aves.email.vo.ActivateUserEmailRequest;
import com.aves.email.vo.EmailRequest;

public interface EmailService {
    
    public int sendEmail(EmailRequest  pEmailReq);
    public int sendActivateUserEmail(ActivateUserEmailRequest  pEmailReq);
    public int sendResetPasswordEmail(ResetPasswordEmailRequest  pEmailReq);
    
}
