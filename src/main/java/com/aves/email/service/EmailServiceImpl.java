package com.aves.email.service;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import com.aves.email.utils.EmailConstants;
import com.aves.email.vo.ActivateUserEmailRequest;
import com.aves.email.vo.EmailRequest;
import com.aves.email.vo.ResetPasswordEmailRequest;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
//import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl implements EmailService{

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private EmailConstants emailConstants;


    public int sendResetPasswordEmail(ResetPasswordEmailRequest  pEmailReq){
        System.out.println("begin EmailServiceImpl:sendResetPasswordEmail()");
        int outputCode = 0;

        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(emailConstants.RESET_PASWD_FROM);
            helper.setTo(pEmailReq.getToAddress());
            helper.setSubject(emailConstants.RESET_PASWD_SUBJECT);   
            helper.setText(getResetPasswordEmailContent(pEmailReq), true);
            emailSender.send(message);
            System.out.println("end EmailServiceImpl:sendResetPasswordEmail()");    
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return outputCode;
    }



    public int sendActivateUserEmail(ActivateUserEmailRequest  pEmailReq){

        System.out.println("begin EmailServiceImpl:sendActivateUserEmail()");
        int outputCode = 0;
        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(emailConstants.ACTIVATE_USER_FROM);
            helper.setTo(pEmailReq.getToAddress());
            helper.setSubject(emailConstants.ACTIVATE_USER_SUBJECT);   
            helper.setText(getActivateUserEmailContent(pEmailReq), true);
            emailSender.send(message);
            System.out.println("end EmailServiceImpl:sendActivateUserEmail()");    
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return outputCode;
    }
 

    public int sendEmail(EmailRequest  pEmailReq){

        System.out.println("begin EmailServiceImpl:sendEmail()");
        int outputCode = 0;

        try{
    
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(pEmailReq.getFromAddress());
            helper.setTo(pEmailReq.getToAddresses());
            helper.setSubject(pEmailReq.getSubject());

            String contentType = pEmailReq.getContentType();
            contentType = (contentType == null) ? "" : contentType.trim();
            if(contentType.contains("html"))
                helper.setText(pEmailReq.getContent(), true);
            else
                helper.setText(pEmailReq.getContent());

        
                emailSender.send(message);
            System.out.println("end EmailServiceImpl:sendEmail()");    
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return outputCode;
    }


    public int sendEmailSimple(EmailRequest  pEmailReq){

        System.out.println("begin EmailServiceImpl:sendEmail()");
        int outputCode = 0;

        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(pEmailReq.getFromAddress());
            message.setTo(pEmailReq.getToAddresses());
            message.setSubject(pEmailReq.getSubject());
            message.setText(pEmailReq.getContent());
            emailSender.send(message);
            System.out.println("end EmailServiceImpl:sendEmail()");    
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return outputCode;
    }

    private String getActivateUserEmailContent(ActivateUserEmailRequest  pEmailReq)
    throws Exception
    {
        VelocityEngine velEngine = getVelocityEngine();
        VelocityContext context = new VelocityContext();    
        context.put("userName", pEmailReq.getUserName());
        context.put("uuid", pEmailReq.getUuid());
        StringWriter stringWriter = new StringWriter();
        velEngine.mergeTemplate("templates/activateUserEmailTemplate.vm", "UTF-8", context, stringWriter);
        String text = stringWriter.toString();
        System.out.println("Text build from template : " + text);
        return text;
    }



    private String getResetPasswordEmailContent(ResetPasswordEmailRequest pEmailReq)
    throws Exception
    {
        VelocityEngine velEngine = getVelocityEngine();
        VelocityContext context = new VelocityContext();    
        StringWriter stringWriter = new StringWriter();
        velEngine.mergeTemplate("templates/passwordResetEmailTemplate.vm", "UTF-8", context, stringWriter);
        String text = stringWriter.toString();
        System.out.println("Text build from template : " + text);
        return text;

    }


    private VelocityEngine getVelocityEngine() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("input.encoding", "UTF-8");
        properties.setProperty("output.encoding", "UTF-8");
        properties.setProperty("resource.loader", "class");
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine velocityEngine = new VelocityEngine(properties);
        return velocityEngine;
    }

}
