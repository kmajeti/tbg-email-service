package com.aves.email.controller;

import com.aves.email.service.EmailService;
import com.aves.email.vo.ActivateUserEmailRequest;
import com.aves.email.vo.EmailRequest;
import com.aves.email.vo.ResetPasswordEmailRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tbg-email")
public class EmailController{

    @Autowired
    private EmailService emailService;

    @GetMapping("/hello1")
    public String getHello()
    {
        return "Hello World";
    }
  
    @PostMapping("/v1")
    public ResponseEntity<Integer>sendEmail(@RequestBody EmailRequest pEmailReq)
    {
        System.out.println("EmailRequest : " + pEmailReq);
        int status = emailService.sendEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/aves-activate-user/v1")
    public ResponseEntity<Integer>sendActivateUserEmail(@RequestBody ActivateUserEmailRequest pEmailReq)
    {
        System.out.println("EmailRequest : " + pEmailReq);
        int status = emailService.sendActivateUserEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/aves-reset-passsword/v1")
    public ResponseEntity<Integer>sendResetPasswordEmail(@RequestBody ResetPasswordEmailRequest pEmailReq)
    {
        System.out.println("EmailRequest : " + pEmailReq);
        int status = emailService.sendResetPasswordEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }


    private boolean validateInputs(EmailRequest pEmailReq){
        boolean result = true;
        return result;
    }
}