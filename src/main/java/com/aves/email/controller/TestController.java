package com.aves.email.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController{
    //@Autowired
    //private EmailService emailService;
    @GetMapping("/one") 
    public String getHello()
    {
        return "Hello World One!";
    }
}