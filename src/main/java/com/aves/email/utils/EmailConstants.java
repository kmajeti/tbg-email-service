package com.aves.email.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EmailConstants{

    @Value("${email.resetPassword.subject}")
    public String RESET_PASWD_SUBJECT;


    
    @Value("${email.resetPassword.from}")
    public String RESET_PASWD_FROM;

    
    @Value("${email.activateUser.from}")
    public String ACTIVATE_USER_FROM;
    
    @Value("${email.activateUser.subject}")
    public String ACTIVATE_USER_SUBJECT;

}



