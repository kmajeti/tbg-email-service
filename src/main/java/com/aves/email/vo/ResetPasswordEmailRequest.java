package com.aves.email.vo;

public class ResetPasswordEmailRequest{

    private String toAddress;

    public String getToAddress()
    {
        return toAddress;
    }

    public void setToAddress(String pStr)
    {
        this.toAddress = pStr;
    }
 
}