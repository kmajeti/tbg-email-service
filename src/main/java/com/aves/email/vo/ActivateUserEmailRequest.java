package com.aves.email.vo;

public class ActivateUserEmailRequest{

    private String toAddress;
    private String userName;
    private String uuid;

    public String toString(){
        return String.join(System.getProperty("line.separator"),
        "ToAddress = " + toAddress,
        "userName = " + userName,
        "uuid = " + uuid);
    }
    
    
    public ActivateUserEmailRequest(){

    }

    public String getToAddress()
    {
        return toAddress;
    }

    public void setToAddress(String pStr)
    {
        this.toAddress = pStr;
    }
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String pStr)
    {
        this.userName = pStr;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String pStr)
    {
        this.uuid = pStr;
    }
}