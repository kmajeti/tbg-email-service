package com.aves.email.vo;

public class EmailRequest{
    
    private String fromAddress;
    private String[] toAddresses;
    private String subject;
    private String content;
    private String contentType;

    public String toString(){
        
        StringBuilder sb = new StringBuilder();
        sb.append(" fromAddress : " + fromAddress);
        sb.append(" toAddresses : " + toAddresses);
        sb.append(" subject : " + subject);
        sb.append(" content : " + content);
        sb.append(" contentType : " + contentType);
        return sb.toString();
    }

    public EmailRequest(){

    }

    public String getFromAddress()
    {
        return fromAddress;
    }

    public void setFromAddress(String pStr)
    {
        this.fromAddress = pStr;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String pStr)
    {
        this.subject = pStr;
    }

    public String[] getToAddresses()
    {
        return toAddresses;
    }

    public void setToAddresses(String[] pStr)
    {
        this.toAddresses = pStr;
    }
    
    public String getContent()
    {
        return content;
    }

    public void setContent(String pStr)
    {
        this.content = pStr;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String pStr)
    {
        this.contentType = pStr;
    }
}